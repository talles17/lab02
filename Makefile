RM= rm -rf
CC=g++

LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test

CFLAGS= -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

.PHONY: all clean doxy debug doc

all: sequencia mdc dec2bin quadrado_recursivo quadrado_interativo ternaria 

debug: CFLAGS += -g -O0
debug: sequencia mdc dec2bin quadrado_recursivo quadrado_interativo ternaria

sequencia: CFLAGS += -I$(INC_DIR)/questao1

sequencia: $(OBJ_DIR)/questao1/func_questao1.o $(OBJ_DIR)/questao1/main_questao1.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/questao1 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'sequencia' criado em $(BIN_DIR)] +++"
	@echo "============="

$(OBJ_DIR)/questao1/func_questao1.o: $(SRC_DIR)/questao1/func_questao1.cpp $(INC_DIR)/questao1/func_questao1.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/questao1/main_questao1.o: $(SRC_DIR)/questao1/main_questao1.cpp
	$(CC) -c $(CFLAGS) -o $@ $<		

mdc: CFLAGS += -I$(INC_DIR)/questao2
mdc: $(OBJ_DIR)/questao2/func_mdc.o $(OBJ_DIR)/questao2/main_questao2.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/questao2 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'mdc' criado em $(BIN_DIR)] +++"
	@echo "============="	

$(OBJ_DIR)/questao2/func_mdc.o: $(SRC_DIR)/questao2/func_mdc.cpp $(INC_DIR)/questao2/func_mdc.h
	$(CC) -c $(CFLAGS) -o $@ $< 

$(OBJ_DIR)/questao2/main_questao2.o: $(SRC_DIR)/questao2/main_questao2.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

dec2bin: CFLAGS += -I$(INC_DIR)/questao3
dec2bin: $(OBJ_DIR)/questao3/func_convbinaria.o $(OBJ_DIR)/questao3/main_questao3.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/questao3 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'dec2bin' criado em $(BIN_DIR)] +++"
	@echo "============="

$(OBJ_DIR)/questao3/func_convbinaria.o: $(SRC_DIR)/questao3/func_convbinaria.cpp $(INC_DIR)/questao3/func_convbinaria.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/questao3/main_questao3.o: $(SRC_DIR)/questao3/main_questao3.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

quadrado_recursivo: CFLAGS += -I$(INC_DIR)/questao5
quadrado_recursivo: $(OBJ_DIR)/questao5/quadrado.o $(OBJ_DIR)/questao5/main1_questao5.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/questao5 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'quadrado_recursivo' criado em $(BIN_DIR)] +++"
	@echo "============="

$(OBJ_DIR)/questao5/quadrado.o: $(SRC_DIR)/questao5/quadrado.cpp $(INC_DIR)/questao5/quadrado.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/questao5/main1_questao5.o: $(SRC_DIR)/questao5/main1_questao5.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

quadrado_interativo: CFLAGS += -I$(INC_DIR)/questao5
quadrado_interativo: $(OBJ_DIR)/questao5/quadrado.o $(OBJ_DIR)/questao5/main2_questao5.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/questao5 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'quadrado_interativo' criado em $(BIN_DIR)] +++"
	@echo "============="

$(OBJ_DIR)/questao5/quadrado.o: $(SRC_DIR)/questao5/quadrado.cpp $(INC_DIR)/questao5/quadrado.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/questao5/main2_questao5.o: $(SRC_DIR)/questao5/main2_questao5.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

ternaria: CFLAGS += -I$(INC_DIR)/questao6
ternaria: $(OBJ_DIR)/questao6/busca_ternaria.o $(OBJ_DIR)/questao6/main_questao6.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/questao6 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'ternaria' criado em $(BIN_DIR)] +++"
	@echo "============="	

$(OBJ_DIR)/questao6/busca_ternaria.o: $(SRC_DIR)/questao6/busca_ternaria.cpp $(INC_DIR)/questao6/busca_ternaria.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/questao6/main_questao6.o: $(SRC_DIR)/questao6/main_questao6.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

doxy: 
	$(RM) $(DOC_DIR)/*
	doxygen -g

doc: 
	doxygen

clean: 
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*