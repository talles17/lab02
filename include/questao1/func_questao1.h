/**
 * @file	func_questao1.h
 * @brief	Arquivo cabecalho contendo a definicao de funcoes usadas
 		    na questao1 do laboratorio 2
 * @author  Cleydson Talles Araujo Vieira
 * @since	16/03/2017
 * @date	19/03/2017

*/

#ifndef FUNC_QUESTAO1_H
#define FUNC_QUESTAO1_H

/**
 * @brief Funcao que retorna o valor da sequencia a recursiva
 * @param n Numero dado pelo usuario
*/
float sequencia_a_recursiva (float n) ;
/**
 * @brief Funcao que retorna o valor da sequencia b recursiva
 * @param n Numero dado pelo usuario
*/

float sequencia_b_recursiva (float n) ;
/**
 * @brief Funcao que retorna o valor da sequencia a interativa
 * @param n Numero dado pelo usuario
*/

float sequencia_a_interativa (float n) ;
/**
 * @brief Funcao que retorna o valor da sequencia b interativa
 * @param n Numero dado pelo usuario
*/
float sequencia_b_interativa (float n ) ;


#endif /* FUNC_QUESTAO1_H */