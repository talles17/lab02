/**
 * @file	busca_ternaria.h
 * @brief	Arquivo cabecalho contendo a definicao de funcoes usadas
 		    na questao6 do laboratorio 2
 * @author  Cleydson Talles Araujo Vieira
 * @since	19/03/2017
 * @date	19/03/2017

*/
#ifndef BUSCA_TERNARIA_H
#define BUSCA_TERNARIA_H

/**
* @brief Funcao que procura o valor x no vetor, e retorna se ele esta ou nao no vetor
* @param v Vetor
* @param ini Indice do inicio do vetor
* @param fim Indice do final do vetor
* @param x Valor do numero dado pelo usuario 
*/
bool busca_ternaria (int v[] , int ini, int fim, int x) ;


#endif /* BUSCA_TERNARIA_H */