/**
 * @file	quadrado.h
 * @brief	Arquivo cabecalho contendo a definicao de funcoes usadas
 		    na questao5 do laboratorio 2
 * @author  Cleydson Talles Araujo Vieira
 * @since	19/03/2017
 * @date	19/03/2017

*/
#ifndef QUADRADO_H
#define QUADRADO_H

/**
 * @brief Funcao que retorna o quadrado de um numero de maneira recursiva
 * @param n Numero dado pelo usuario
*/
int quadrado_rec (int n) ;
/**
 * @brief Funcao que retorna o quadrado de um numero de maneira interativa
 * @param n Numero dado pelo usuario
*/
int quadrado_int (int n) ;

#endif /* QUADRADO_H */