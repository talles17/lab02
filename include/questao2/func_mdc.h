/**
 * @file	func_mdc.h
 * @brief	Arquivo cabecalho contendo a definicao de funcoes usadas
 		    na questao2 do laboratorio 2
 * @author  Cleydson Talles Araujo Vieira
 * @since	16/03/2017
 * @date	19/03/2017

*/

#ifndef FUNC_MDC_H
#define FUNC_MDC_H

/**
* @brief Funcao que retorna o mdc de a e b atraves do algoritmo de euclides
* @param a Numero dado pelo usuario
* @param b Numero dado pelo usuario
*/
int algoritimo_de_euclides (int a , int b) ;


#endif /* FUNC_MDC_H */