#include <iostream>

#include "func_convbinaria.h"

int * decimal_binario (int n,  int i ) {
	int  q ;
	int *v = (int *) calloc (n, sizeof(int)) ;
	q = n / 2 ;
	v[i] = n % 2 ;
	if (q == 0) {
		return v ;
	}

	else {
		return decimal_binario (q , i+1) ;
	}
}