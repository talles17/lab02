/**
 * @file	func_mdc.cpp
 * @brief	Arquivo de corpo contendo a implementacao da funcao que realiza
 			o algoritimo de euclides
 * @author  Cleydson Talles Araujo Vieira
 * @since	16/03/2017
 * @date	19/03/2017
 * @sa 		func_mdc.h

*/
#include <iostream>

#include "func_mdc.h"
/**
* @brief Funcao que retorna o mdc de a e b atraves do algoritmo de euclides
* @param a Numero dado pelo usuario
* @param b Numero dado pelo usuario
*/
int algoritimo_de_euclides (int a , int b) {
	int resto = 1  ;
	
	if (b > a) {
		int aux = b ;
		b = a ;
		a = aux ;
	}
	
	resto = a % b ;
	a = b ;
	b = resto ;

	if (resto == 0) {
		return a ;
	}

	else {
		return algoritimo_de_euclides (a , b) ;
	}

	/*while (resto != 0) {
		resto = a % b ;
		a = b ;
		b = resto ;

	} */

	return a ;
}

