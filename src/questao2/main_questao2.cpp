/**
 * @file	main_questao2.cpp
 * @brief	Codigo fonte das funcoes usadas na questao 2 
 			do laboratorio 2
 * @author  Cleydson Talles Araujo Vieira
 * @since	17/03/2017
 * @date	19/03/2017
 * @sa 		func_mdc.h

*/


#include <iostream>

#include "func_mdc.h"

using std::cout ;
using std::endl ;
using std::cin ;
/* @brief Funcao principal */
int main () {
	int n1 , n2 , mdc ;

	cout << "Digite dois numeros naturais positivos: " ;
	cin >> n1 >> n2 ;
	/* Testa se ambos os numeros sao maiores que 0, para poder aplicar o algoritimo de euclides */
	if (n1 > 0 && n2 > 0) {
		mdc = algoritimo_de_euclides (n1 , n2) ;

		cout << "MDC("<< n1 << "," << n2 << ") = " << mdc << endl ;
	}
	/* Se algum dos numeros for menor que 0, da uma mensagem de erro */
	else {
		cout << "Numeros invalidos!" << endl ; ;
	}

	return 0 ;
} 