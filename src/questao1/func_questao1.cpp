/**
 * @file	func_questao1.cpp
 * @brief	Arquivo de corpo contendo a implementacao das funcoes que realizam
 			a sequencia a ou b, de modo recursivo ou interativo
 * @author  Cleydson Talles Araujo Vieira
 * @since	16/03/2017
 * @date	19/03/2017
 * @sa 		func_questao1.h

*/

#include <iostream>
#include <math.h>
#include "func_questao1.h"

/**
 * @brief Funcao que retorna o valor da sequencia a recursiva
 * @param n Numero dado pelo usuario
*/
float sequencia_a_recursiva (float n) {
	if (n == 1) {
		return 1 ;
	}
	else {
		return 1/n + sequencia_a_recursiva (n-1) ;
	}
}
/**
 * @brief Funcao que retorna o valor da sequencia b recursiva
 * @param n Numero dado pelo usuario
*/ 

float sequencia_b_recursiva (float n) {
	if (n == 1) {
		return ((pow(n,2) + 1 )/ (n + 3)) ;
	}
	else {
		return ((pow(n,2) + 1) / (n + 3)) + sequencia_b_recursiva (n-1) ;
	}
}
/**
 * @brief Funcao que retorna o valor da sequencia a interativa
 * @param n Numero dado pelo usuario
*/

float sequencia_a_interativa (float n) {
	float saida = 0 ;

	while (n!=0) {
		saida += 1/n ;
		n-- ; 
	} 

	return saida ;
}
/**
 * @brief Funcao que retorna o valor da sequencia b interativa
 * @param n Numero dado pelo usuario
*/

float sequencia_b_interativa (float n ) {
	float saida = 0 ;

	while (n != 0) {
		saida += ((pow(n,2) + 1) / (n + 3)) ;
		n-- ; 
	}

	return saida ;
}
