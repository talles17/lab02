/**
 * @file	main_questao1.cpp
 * @brief	Codigo fonte das funcoes usadas na questao 1 
 			do laboratorio 2
 * @author  Cleydson Talles Araujo Vieira
 * @since	16/03/2017
 * @date	19/03/2017
 * @sa 		func_questao1.h

*/

#include <iostream>
#include <cstdlib>

#include "func_questao1.h"

using std::cout ;
using std::endl ;
using std::cin ;

/** @brief Funcao principal */
int main (int argc, char *argv[]) {
	float numero , saida ;

	if (argc == 4) {
		/*Passagem do valor a ser calculado */
		numero = atoi(argv[3]) ;
		/* Verifica se os parametros estao corretos */
		if (*argv[1] == 'A' || *argv[1] == 'a'){
			
			if (*argv[2] == 'R' || *argv[2] == 'r') {
				saida = sequencia_a_recursiva (numero) ;
				cout << "O valor da sequencia A para N = " << numero <<" e " << saida <<" (a versao recursiva foi usada)" << endl;
			}
			if (*argv[2] == 'I' || *argv[2] == 'i') {
				saida = sequencia_a_interativa (numero) ;
				cout << "O valor da sequencia A para N = " << numero <<" e " << saida <<" (a versao interativa foi usada)" << endl;
			}

		}
		/* Verifica se os parametros estao corretos */
		if (*argv[1] == 'B' || *argv[1] == 'b') {

			if (*argv[2] == 'R' || *argv[2] == 'r') {
				saida = sequencia_b_recursiva (numero) ;
				cout << "O valor da sequencia B para N = " << numero <<" e " << saida <<" (a versao recursiva foi usada)" << endl;
			}
			if (*argv[2] == 'I' || *argv[2] == 'i') {
				saida = sequencia_b_interativa (numero) ;
				cout << "O valor da sequencia B para N = " << numero <<" e " << saida <<" (a versao interativa foi usada)" << endl;
			}

		}

	}
	/* Da uma mensagem de erro caso os parametros estejam incorretos */
	else {
		cout << "Erro !, utilize a ou b para sequencia, r ou i para funcao e qualquer numero inteiro." << endl ;

	}

	return 0 ;
}