#include <iostream>
#include <cstdlib>
#include <string>

#include "func_palindromos.h"

int verifica_palindromos (int i , char palavra[] , int j , char contrario[] ) {
	
	
	if (palavra[i] != contrario[j]) {
		return 1 ;
	}
	if (i > j || i == j) {
		return 2 ;
	}
	else {
		return verifica_palindromos (i+1, palavra, j-1, contrario) ;
	}

}