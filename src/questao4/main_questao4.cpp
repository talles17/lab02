#include <iostream>
#include <cstdlib>
#include <string>

#include "func_palindromos.h"

using namespace std ;

int main () {
	int i , j , saida  ;
	char palavra[50] , contrario[50] ;

	cout << "Digite uma palavra: " ;
	getline(cin, palavra) ;

	j = palavra.size() ;
	j-- ;
 
	for (i = 0 ; i < j ; i++) {
		contrario[i] = palavra[i] ;
	} 

	i = 0 ;
	
	saida = verifica_palindromos (i, palavra , j , contrario) ;

	if (saida == 1) {
		cout << "'' " << palavra << "'' nao e um palindromo" << endl ;
	}

	if (saida == 2) {
		cout << "'' " << palavra << "'' e um palindromo" << endl ;
	}


	return 0 ;

}