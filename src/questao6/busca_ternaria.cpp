/**
 * @file	busca_ternaria.cpp
 * @brief	Arquivo de corpo contendo a implementacao da funcao que realiza
 			a busca de um numero em um vetor
 * @author  Cleydson Talles Araujo Vieira
 * @since	19/03/2017
 * @date	19/03/2017
 * @sa 		busca_ternaria.h

*/


#include <iostream>

#include "busca_ternaria.h"

/**
* @brief Funcao que procura o valor x no vetor, e retorna se ele esta ou nao no vetor
* @param v Vetor
* @param ini Indice do inicio do vetor
* @param fim Indice do final do vetor
* @param x Valor do numero dado pelo usuario 
*/
bool busca_ternaria (int v[] , int ini, int fim, int x) {

	if (fim < ini) {
		return false ;
	}

	int indice1 = (fim - ini)/3 + ini ;
	int indice2 = 2 * ((fim - ini)/3 + ini) ;
	int valorMemoria1 = v[indice1] ;
	int valorMemoria2 = v[indice2] ;

	if (valorMemoria1 == x ) {
		return true ;
	}

	else if (valorMemoria2 == x) {
		return true ;
	}

	else if (valorMemoria1 > x) {
		return busca_ternaria (v , ini , indice1 - 1 , x) ;
	}

	else if (valorMemoria2 < x) {
		return busca_ternaria (v , indice2 + 1 , fim , x) ;
	}

	else {
		return busca_ternaria (v , indice1, indice2 - 1 , x) ;
	}

}