/**
 * @file	main_questao6.cpp
 * @brief	Codigo fonte das funcoes usadas na questao 6 
 			do laboratorio 2
 * @author  Cleydson Talles Araujo Vieira
 * @since	19/03/2017
 * @date	19/03/2017
 * @sa 		busca_ternaria.h

*/

#include <iostream>

#include "busca_ternaria.h"

using std::cout ;
using std::endl ;
using std::cin ;

int main (int argc, char *argv[]) {
	int v[26] = {2 , 5 , 9 , 11 , 13 , 17 , 22 , 24 , 33 , 38 , 39 , 40 , 45 , 
	56 , 71 , 99 , 110 , 113 , 132 , 155 , 166 , 203 , 211 , 212 , 230 , 233 } ;
	int inicio = 0 , fim = 25 , n , resultado ;

	n = atoi (argv[1]) ;

	resultado = busca_ternaria (v , inicio , fim , n) ;
	if (resultado == 0) {
		cout << "O elemento " << n << " nao faz parte do vetor" << endl ;
	}

	if (resultado == 1) {
		cout << "O elemento " << n << " faz parte do vetor" << endl ;
	}


	return 0 ;

}