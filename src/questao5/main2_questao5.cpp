/**
 * @file	main2_questao5.cpp
 * @brief	Codigo fonte das funcoes usadas na questao 5 
 			do laboratorio 2
 * @author  Cleydson Talles Araujo Vieira
 * @since	19/03/2017
 * @date	19/03/2017
 * @sa 		quadrado.h

*/
#include <iostream>

#include "quadrado.h"

using std::cout ;
using std::endl ;
using std::cin ;

/* @brief Funcao principal */
int main (int argc, char *argv[]) {
	int n , quad , j = 1 ;

	n = atoi (argv[1]) ;
	/* Testa se o numero e maior que 0, para poder aplicar a funcao que calcula o quadrado */
	if (n > 0) {

		quad = quadrado_int (n) ;
		cout << "quadrado(" << n <<") => " << j ;
		for (int i=1 ; i < n ; i++) {
			j+= 2 ;
			cout <<" + " << j ;
		}

		cout <<" = " << quad << endl ;
	}
	/* Se o numero dado pelo usuario for igual ou menor do que zero, sai uma mensagem de erro */
	else {
		cout << "Numero invalido!" << endl ;
	}


	return 0 ;

}