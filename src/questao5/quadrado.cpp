/**
 * @file	quadrado.cpp
 * @brief	Arquivo de corpo contendo a implementacao da funcao que realiza
 			o calculo do quadrado de um numero
 * @author  Cleydson Talles Araujo Vieira
 * @since	19/03/2017
 * @date	19/03/2017
 * @sa 		quadrado.h

*/
#include <iostream>

#include "quadrado.h"
/**
 * @brief Funcao que retorna o quadrado de um numero de maneira recursiva
 * @param n Numero dado pelo usuario
*/
int quadrado_rec (int n) {

	if (n == 1) {	
		return 1 ;
	}

	else {
		return ((2*n )- 1) + quadrado_rec (n-1) ;
	}
}
/**
 * @brief Funcao que retorna o quadrado de um numero de maneira interativa
 * @param n Numero dado pelo usuario
*/
int quadrado_int (int n) {
	int x = 0 ;
	
	while (n != 0) {
		x += ((2*n )- 1) ;
		n-- ; 
	}

	return x ;

}